// Copyright 2017 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package syslog

import (
	"io"
	"net"
	"os"
	"time"
)

type dummyConn struct {
	io.Writer
}

func (dummyConn) Read(b []byte) (n int, err error) {
	panic("read not implemented")
}

func (dummyConn) Close() error {
	return nil
}

func (dummyConn) LocalAddr() net.Addr {
	panic("LocalAddr not implemented")
}

func (dummyConn) RemoteAddr() net.Addr {
	panic("RemoteAddr not implemented")
}

func (dummyConn) SetDeadline(t time.Time) error {
	panic("SetDeadline not implemented")
}

func (dummyConn) SetReadDeadline(t time.Time) error {
	panic("SetReadDeadline not implemented")
}

func (dummyConn) SetWriteDeadline(t time.Time) error {
	panic("SetWriteDeadline not implemented")
}

// NewWriter writes syslog formatted messages to an underlying io.Writer.
//
// If the priority is invalid, NewWriter panics.
func NewWriter(w io.Writer, priority Priority, tag string) *Writer {
	if priority < 0 || priority > LOG_LOCAL7|LOG_DEBUG {
		panic("syslog: invalid priority")
	}

	if tag == "" {
		tag = os.Args[0]
	}
	hostname, _ := os.Hostname()

	network := "unknown"
	raddr := ""
	var logConn net.Conn
	if conn, ok := w.(net.Conn); ok {
		addr := conn.RemoteAddr()
		network = addr.Network()
		raddr = addr.String()
		logConn = conn
	}

	if logConn == nil {
		logConn = dummyConn{w}
	}

	return &Writer{
		priority: priority,
		tag:      tag,
		hostname: hostname,
		network:  network,
		raddr:    raddr,
		conn:     &netConn{local: true, conn: logConn},
	}
}

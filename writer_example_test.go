// Copyright 2017 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package syslog_test

import (
	"fmt"
	"os"

	"code.soquee.net/syslog"
)

func ExampleNewWriter() {
	sysLog := syslog.NewWriter(os.Stdout,
		syslog.LOG_WARNING|syslog.LOG_DAEMON, "demotag")
	fmt.Fprintf(sysLog, "This is a daemon warning with demotag.")
	sysLog.Emerg("This is a daemon emergency with demotag.")
}
